import Navbar from "./navbar";
import Title from "./title";

export default function Layout(props) {
    console.log(props)
    return (
        <div>
            <Navbar></Navbar>
            <main>
                <Title>{props.title}</Title>
                { props.children }
            </main>
            <style jsx>
                {`
                  div {
                    min-height: 100vh;
                    display: flex;
                    flex-direction: column;
                    justify-content: center;
                    justify-items: center;
                  }
                  
                `}
            </style>
            <style jsx global>
                {`
                  html, body {
                    padding: 0;
                    margin: 0;
                  }
                `}
            </style>
        </div>
    )
}