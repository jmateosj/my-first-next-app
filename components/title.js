export default function Title({ children }) {
    return (
        <div>
            <h1>{children}</h1>
            <style jsx>
                {`
                  margin: 0;
                  font-size: 3rem;
                  text-align: center;
                  color: red;
                `}
            </style>
        </div>
    )
}