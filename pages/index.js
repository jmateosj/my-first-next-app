import Head from 'next/head'
import styles from '../styles/Home.module.css'
import Navbar from "../components/navbar";
import Title from "../components/title";
import Layout from "../components/layout";

export default function Home() {
  return (
      <Layout title={'HOME'}>
            <p>This is my HOME page</p>
      </Layout>
  )
}
