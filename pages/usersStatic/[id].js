import { useRouter } from "next/router";
import Navbar from "../../components/navbar";
import Layout from "../../components/layout";
import React from "react";

export default function User({ user }) {
    // router accede a la request con toda la información
    const router= useRouter()

    if (router.isFallback) {
        return <div>Cargando...</div>
    }

    console.log(router)
    return (
        <Layout title={'User Details'}>
            <div className='card'>
                <h3>ID {user?.id}</h3>
                <p>Email: {user?.email}</p>
                <p>Username: {user?.username} </p>
            </div>
            <style jsx>
                {`                        
        .card {
          margin: 1rem;
          flex-basis: 45%;
          padding: 1.5rem;
          text-align: left;
          color: inherit;
          text-decoration: none;
          border: 1px solid #eaeaea;
          border-radius: 10px;
          transition: color 0.15s ease, border-color 0.15s ease;
        }
        .card:hover,
        .card:focus,
        .card:active {
          color: #0070f3;
          border-color: #0070f3;
        }
        .card h3 {
          margin: 0 0 1rem 0;
          font-size: 1.5rem;
        }
        .card p {
          margin: 0;
          font-size: 1.25rem;
          line-height: 1.5;
        }
      `}
            </style>
        </Layout>
    )
}

export async function getStaticPaths() {
    // podríamos hacer fetch para tener todos los paths prerenderizados
    const paths = [
        { params: {id: '1'}},
        { params: {id: '2'}},
    ];
    return {
        paths,
        // con false va a 404; con true lo tramita next automáticamente
        fallback: true
    }
}

export async function getStaticProps(context){
    // context.params
    // context.params.id
    const res = await fetch(`https://jsonplaceholder.typicode.com/users/${context.params.id}`);
    const user = await res.json();

    return {
        props: {
            user
        }
    }
}