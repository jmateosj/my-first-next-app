import { useRouter } from "next/router";
import Navbar from "../../components/navbar";
import Layout from "../../components/layout";

export default function id() {
    // router accede a la request con toda la información
    const router= useRouter()
    console.log(router)
    return (
        <Layout title={'User Details'}>
            <p>User ID: {router.query.id} </p>
        </Layout>
    )
}