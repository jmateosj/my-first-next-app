import Layout from "../../components/layout";
import React from 'react'
import Link from "next/link";

export default function Index() {
    //hook para controlar los usuarios y el cambio de estado
    const [users, setUsers] = React.useState([]);

    // CONTENIDO GENERADO POR CLIENTE
    // con el array vacio como segundo parametro solo correra cuando se cargue el componente
    React.useEffect(() => {
        const fetchUsers = async () => {
            const res = await fetch('https://jsonplaceholder.typicode.com/users');
            const newUsers = await res.json();
            setUsers(newUsers);
        };
        fetchUsers();
    }, [])

    return(
        <Layout title={'USERS'}>
            <div>
                {users.map(user => {
                    return (
                        <Link href={'/users/[id]'} as={`/users/${user.username}`} key={user.id}>
                            <a>
                                <h3>{user.name}</h3>
                                <p>{user.email}</p>
                            </a>
                        </Link>
                    )
                })}
            </div>
        </Layout>
    )
}